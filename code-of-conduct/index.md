---
layout: page
title: Código de Conduta
excerpt: "Código de Conduta RubyFloripa"
modified: 2018-05-13T17:58:38.564948-03:00
---

O código de conduta tem o objetivo de proteger o público de danos e perigos morais e deve ser seguido por todas as pessoas envolvidas no evento: participantes, palestrantes, monitores/as, coordenadores/as e colaboradores/as.

Não é tolerado qualquer forma de violência, assédio, intimidação ou difamação de qualquer participante. Atos de violência de gênero, etnia e/ou orientação sexual, assédio sexual e/ou moral serão veementemente rechaçados. Não será permitido o uso de imagens e materiais com conteúdos sexuais, homofóbicos, racistas e/ou discriminatórios.

Por assédio entende-se, sem limitação: comentários ofensivos (verbais ou eletrônicos) relacionados a características ou escolhas pessoais, etnia, gênero, bem como comentários ou imagens sexuais, racistas, homofóbicos ou discriminatórios de qualquer natureza em espaços presenciais ou digitais; intimidação deliberada, perseguição e encalço; fotografias ou gravações que gerem embaraço; interrupções reiteradas de palestras por motivos não relacionados aos conteúdos das mesmas; contato físico inadequado ou atenção sexual indesejada.

Ao notar ou ser reportada a ocorrência de algum comportamento que infrinja o código de conduta, o/a responsável pela realização do ato será advertido/a a descontinuar as ações que estejam violando o código. Em caso de reincidência, este/esta poderá ser convidado/a a se retirar do evento a critério exclusivo dos organizadores. A organização também se
reserva ao direito de não permitir a participação do/a(s) envolvido/a(s) nas demais edições do evento.

Se você notar ou for vítima de qualquer tipo de violência (verbal ou física) ou assédio durante o evento, por favor, relate o ocorrido à organização ou qualquer representante do RubyFloripa. Organizadores/as e colaboradores/as do evento serão atentos e zelosos para cuidar e evitar, da melhor forma possível, todo tipo de situação que possa causar
constrangimentos.

### Licença

Acreditamos no senso de comunidade e no espírito do software livre em que todos temos o direito de executar, copiar, distribuir, estudar, mudar, melhorar, por isso nosso Código de Conduta está sob a Atribuição 4.0 Internacional (CC BY 4.0). Essa é a licença mais flexível de todas disponíveis. Isso quer dizer que você tem o direito de:

Copiar e redistribuir o material em qualquer suporte ou formato;
Adaptar: remixar, transformar e criar a partir do material para qualquer fim, mesmo que para uso comercial.

O licenciante não pode revogar estes direitos desde que você respeite os termos da licença.

### Referências

* [Codamos](https://www.codamos.club/comunidades)
* [The Developers Conference](http://www.thedevelopersconference.com.br/conduta)
* [Rails Conf](https://railsconf.com/policies)
* [Rails Girls](http://railsgirls.com/munich_coc.html)
* [Moodle Brasil](https://www.moodlebrasil.org/mod/page/view.php?id=464)



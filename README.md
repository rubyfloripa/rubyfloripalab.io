# RubyFloripa

Esse é o código do site do [RubyFloripa](https://ruby.floripa.br).

Ele é feito com [Jekyll](http://jekyllrb.com) se você quiser colaborar :wink:

Se você quiser algumas informações rápidas:

* Site oficial é o https://ruby.floripa.br
* Siga o [@rubyfloripa no twitter](https://twitter.com/rubyfloripa)
* Estamos no [Github](https://github.com/rubyfloripa) e no [Gitlab](https://gitlab.com/rubyfloripa)

## Setup

Install dependencies:

```
bundle install
```

Run `jekyll` server:

```
bin/jekyll server
```

Now access [http://127.0.0.1:4000](http://127.0.0.1:4000)

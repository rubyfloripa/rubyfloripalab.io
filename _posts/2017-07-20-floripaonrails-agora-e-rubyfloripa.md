---
layout: post
title: "FloripaOnRails agora é RubyFloripa"
excerpt: "Saiba detalhes do novo nome do nosso grupo de Meetups"
categories: articles
tags: [ruby, floripaonrails, meetup, rubyfloripa]
comments: true
share: true
---

Há um tempo já estavámos discutindo sobre o nome `FloripaOnRails` remeter muito ao framework `RubyOnRails` ao invés da linguagem `Ruby`, estavámos querendo desassociar e trazer mais pessoas para comunidade.

Depois da [notícia do NIC](http://nic.br/noticia/releases/joao-pessoa-florianopolis-e-porto-alegre-serao-as-primeiras-cidades-a-terem-dominios-exclusivos-sob-o-br/) (Núcleo de Informação e Coordenação do Ponto BR) decidimos que seria uma boa hora para pegar o domínio [https://ruby.floripa.br](https://ruby.floripa.br). Renomeamos então todas as referências antigas de FloripaOnRails para RubyFloripa.

---

Quer ficar por dentro dos próximos meetups? [Faça parte da nossa comunidade](/faca-parte).

Também se inscreva no [canal RubyFloripa no Youtube](https://www.youtube.com/channel/UCnSXXzLeSmpmTILPdkkjDfg) e confira o que rolou até agora.


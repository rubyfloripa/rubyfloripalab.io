---
layout: post
title: "1º Meetup RubyFloripa"
excerpt: "Saiba como surgiu o RubyFloripa e confira as tech talks apresentadas no dia."
categories: articles
tags: [rails, slides, meetups]
comments: true
share: true
---

Percebendo que Floripa tinha alguns eventos bem estruturados, como o [FloripaJS](http://floripajs.org/), queríamos reunir a comunidade Ruby, que estava bastante dispersa, porém não haviam encontros e nem empresas apoiando tal iniciativa.

Nossa a ideia então, foi de reunir as pessoas/empresas focando em palestras, troca de conhecimento e bastante networking. Nasceu então o [RubyFloripa](http://www.meetup.com/rubyfloripa) e, no dia 7 de Julho de 2015, ocorreu nosso [primeiro meetup RubyFloripa](http://www.meetup.com/rubyfloripa/events/223349050/) no [Sebrae SC](http://www.sebrae.com.br/sites/PortalSebrae/ufs/sc?codUf=25), contando com cerca de 32 pessoas:

![Galera do RubyFloripa no primeiro meetup](http://photos2.meetupstatic.com/photos/event/7/0/7/4/600_439528788.jpeg)

Tivemos ótimas talks, começando com o pé direito:

### Ruby Code Review

Jônatas Paganini - @jonatasdp

{% slideshare 1FD1Sni9qGcJ1M %}

### ServerSpec

Lucas Alencar - @lucasandre

{% speakerdeck 4f592e23531041e890727426b89235b6 %}

### Dicas e boas práticas para projetos Rails

Nando Sousa - @nandosousafr

{% speakerdeck 64290d000a8f4f73991dd97013028305 normal %}

### Brincando com FFI no Ruby

Weverton Timoteo - @wevtimoteo

{% slideshare CQV8pnqNZMwA49 normal %}


---

Logo no primeiro evento deu para ver a galera bem animada trocando muita informação e fazendo bastante networking, cumprindo exatamente a proposta do meetup. Rolou até pizza no CoffeeBreak servindo até de janta para muitos \o/.

![CoffeeBreak RubyFloripa](http://photos2.meetupstatic.com/photos/event/8/5/1/5/600_439534069.jpeg)

---

Quer ficar por dentro dos próximos meetups? [Faça parte da nossa comunidade](/faca-parte).

Também se inscreva no [canal RubyFloripa no Youtube](https://www.youtube.com/channel/UCnSXXzLeSmpmTILPdkkjDfg) e confira o que rolou até agora.

---
layout: post
title: "28º Encontro - Online"
excerpt: "O que rolou na 28º edição do RubyFloripa com as talks apresentadas no dia."
categories: articles
tags: [ruby, slides, meetup, networking, monads, postgresql, codereview, legado]
comments: true
share: true
---

O 28º encontro do RubyFloripa aconteceu online e foi transmitido pelo Youtube:


{% youtube _bYpuJs1X2k %}

Contamos com o apoio da [RD Station](https://medium.com/rd-shipit)!

Aproveite e [se inscreva](http://www.youtube.com/c/RubyFloripa)!

---

### Aprendizados ao manter uma aplicação Rails de 10 anos no ar

André Leal - @andrehjr

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTdGmhz-tfngOCfep1RRdju14CXB_EusRajMhA1HhO9y067imO8AV_2wPNo6WjNVF9XvDjIT2zlBjzX/embed" style="width:100%; height:440px; border:0;"></iframe>

### Descobrindo Mônadas com Ruby

Gabriela Mafra - @gabrielammafra

Slides disponíveis em seu site pessoal: [GabrielaMafra.dev - Ruby Monads](https://gabrielamafra.dev/presentations/ruby_monads.html)

### Revisão de Código - Uma prática que depende da cultura

Leandro "Pará" - @parazito

{% slideshare d3jltGiOiemTuY normal %}

### Do Ruby ao PostgreSQL: Criando um scanner de informações privadas

Jônatas Paganini - @jonatasdp

Como a talk foi live-coding, o código apresentado está disponível no [Gist](https://gist.github.com/jonatas/7af006a3eddb630cae3b4e2aaba528f9).

---

### Sorteio

No final do evento sorteamos através da RD Station um ingresso premium do TDC \o/

---

28º Encontro - RubyFloripa - Online [Meetup.com](https://www.meetup.com/rubyfloripa/events/277092654/).

Quer ficar por dentro dos próximos meetups? Faça parte da nossa comunidade em [RubyFloripa no Meetup.com](http://www.meetup.com/rubyfloripa).

Também se inscreva no [canal RubyFloripa no Youtube](https://www.youtube.com/c/rubyfloripa) e confira o que rolou até agora.



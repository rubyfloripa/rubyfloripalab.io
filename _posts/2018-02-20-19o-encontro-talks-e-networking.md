---
layout: post
title: "19º Encontro - Talks e Networking"
excerpt: "O que rolou na 19ª edição do RubyFloripa com as talks apresentadas no dia."
categories: articles
tags: [ruby, slides, meetup, networking]
comments: true
share: true
---

O 19º encontro do RubyFloripa aconteceu na [Resultados Digitais](https://www.resultadosdigitais.com.br) e foi gravado, quem quiser assistir pode acessar a [gravação no Zoom](https://zoom.us/recording/play/22WCoQtIuWU8glPU8InhqQ2dAmIoATMsDX50PTVNsBKodC6GhvM8Fc9GWjTYmalj).

### Search Ruby code with fast

Jonatas Paganini - @jonatasdp

O Live coding pode ser assistido na gravação.

### Ferramentas para manter seu código Ruby on Rails limpo e otimizado

[Rafael Besen](https://github.com/rbesen)

{% slideshare Amjl05FfHQ6uHr %}

### Como o code review pode fazer bem para sua equipe

Vinicius Alonso - @alonsoemacao

{% speakerdeck 9080b85f5aa74cd6b3b1f2f73fed8444 %}

---

19º Encontro - Talks e Networking no [Meetup.com](https://www.meetup.com/rubyfloripa/events/247310427/).

Quer ficar por dentro dos próximos meetups? [Faça parte da nossa comunidade](/faca-parte).

Também se inscreva no [canal RubyFloripa no Youtube](https://www.youtube.com/channel/UCnSXXzLeSmpmTILPdkkjDfg) e confira o que rolou até agora.

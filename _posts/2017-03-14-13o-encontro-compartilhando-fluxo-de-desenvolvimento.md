---
layout: post
title: "13º Encontro - Compartilhando fluxo de desenvolvimento"
excerpt: "O que rolou na 13ª edição do RubyFloripa com as talks apresentadas no dia."
categories: articles
tags: [ruby, slides, meetup, workflow, development]
comments: true
share: true
---

O 13º encontro do RubyFloripa aconteceu na [Celta (Parque Tecnológico)](http://www.celta.org.br), HQ da [GeekHunter](http://geekhunter.com.br).

### Benchmarking

Karla Garcia - @karlamariag

{% youtube VMRuw2Tng2I %}

### IRB - PRY e Code search (ag e grep)

Jonatas Paganini (@jonatasdp) e Leandro Parazito (@parazito)

{% youtube EDhMgwAgQB8 %}

### Rubocop e Pronto

Jonatas Paganini (@jonatasdp) e [Rafael Besen](https://github.com/rbesen)

{% youtube ORxTXXnJpWw %}

### Test Driven Development

Guilherme Matsumoto - @g8M

{% youtube NVyEyOkLads %}

### Tmux

Weverton Timoteo (@wevtimoteo) e Nando Sousa (@nandosousafr)

{% youtube maPkweEi8BQ %}

---

13º Encontro - Compartilhando fluxo de desenvolvimento no [Meetup.com](https://www.meetup.com/rubyfloripa/events/237861972/).

Quer ficar por dentro dos próximos meetups? [Faça parte da nossa comunidade](/faca-parte).

Também se inscreva no [canal RubyFloripa no Youtube](https://www.youtube.com/channel/UCnSXXzLeSmpmTILPdkkjDfg) e confira o que rolou até agora.

---
layout: post
title: "[video] Live Coding - Introducão Ruby, Rails e TDD"
excerpt: "No dia 18 de abril ocorreu um live coding no RubyFloripa para iniciantes."
categories: articles
tags: [video, meetups]
comments: true
share: true
---

No dia 18 de abril ocorreu um live coding no RubyFloripa para iniciantes. A
ideia do evento foi mostrar um pouco do cenário geral do desenvolvimento com Rails.

Nessa linha, organizamos a agenda no seguinte formato:

- 30 minutos de **apresentação da sintaxe** com @jonatasdp
- 30 minutos para **conhecer um pouco de Rails** com @williamweckl
- 45 minutos para **programar TDD** com @g8m
- 15 minutos **deploy em produção** com @jaisonerick

{% youtube UxAnqZ5_zSI %}

O evento foi generosamente gravado e editado pelo @derekstavis!

Depois do evento o pessoal teve a oportunidade de fazer muito networking como sempre :)

Nosso próximo meetup será o [sabadão on Rails](http://www.meetup.com/rubyfloripa/events/230489351) possívelmente faremos live stream também!

Fique ligado! Siga [@rubyfloripa no twitter](https://twitter.com/rubyfloripa) e receba os links do stream :)

---
layout: post
title: "25º Encontro - Talks e Networking"
excerpt: "O que rolou na 25º edição do RubyFloripa com as talks apresentadas no dia."
categories: articles
tags: [ruby, comunicacao, slides, meetup, networking, nodejs, rails, mulheres, tecnologia]
comments: true
share: true
---

O 25º encontro do RubyFloripa aconteceu na [HostGator](https://www.hostgator.com.br/).

A HostGator nos apoiou com o local e também patrocinou o nosso coffee break.

O meetup foi transmitido ao vivo e a gravação está disponível no nosso canal do YouTube!

{% youtube PMEwqnbIEvI %}

Aproveite e [se inscreva](http://www.youtube.com/c/RubyFloripa)!

---

Seguem as palestras apresentadas no dia:

### Mulheres no mundo da Tecnologia

Thienne Czizeweski - @thienneczi

<iframe src="https://docs.google.com/file/d/1K5Y6r9TtqA4u0OXPlMbiqZ3qYL_bejF6/preview" style="width:100%; height:440px; border:0;"></iframe>

### O que aprendi na transição de Rails para NodeJS por Lucas Krämer

Lucas Krämer - [@lucaskds](https://github.com/lucaskds)

<iframe src="https://docs.google.com/presentation/d/1dRx1F0JwwCzkCYNLif2ju0jrUWHtqpJDmmNWguJh9Po/preview" style="width:100%; height:440px; border:0;"></iframe>

### O que aprendi lançando 4 produtos em 3 meses

Felipe Bonetto - [@FeSens](https://github.com/FeSens)

<iframe src="https://docs.google.com/presentation/d/1vgYDIeBm9bkL6BVexrWM4VeLBGqsrE-ghV4Ys5kMfHY/preview" style="width:100%; height:440px; border:0;"></iframe>

---

25º Encontro - RubyFloripa - Talks e Networking [Meetup.com](https://www.meetup.com/rubyfloripa/events/261465178/).

Quer ficar por dentro dos próximos meetups? [Faça parte da nossa comunidade](/faca-parte).

Também se inscreva no [canal RubyFloripa no Youtube](https://www.youtube.com/c/rubyfloripa) e confira o que rolou até agora.

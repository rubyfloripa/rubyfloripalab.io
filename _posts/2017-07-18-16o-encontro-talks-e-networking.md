---
layout: post
title: "16º Encontro - Talks e Networking"
excerpt: "O que rolou na 16ª edição do RubyFloripa com as talks apresentadas no dia."
categories: articles
tags: [ruby, slides, meetup, networking]
comments: true
share: true
---

O 16º encontro do RubyFloripa aconteceu no [CIASC](http://www.ciasc.sc.gov.br/).

### Rails Vs Queries Avançadas: Como a convivência entre eles pode ser melhor utilizando Arel

João Paulo Lethier - @jplethier

{% slideshare ipYtmVeEdAukJF %}

### Dicas de segurança: Algumas ameaças e como evitá-las

[Rafael Besen](https://github.com/rbesen)

{% slideshare eyiwufRDl5FWCn %}

### Usando o Vue.js como alternativa front-end na construção de sua aplicação Rails

Christian Nascimento - @cnascimentobr

[Slides](https://docs.google.com/presentation/d/1haIE7dgfw353yO5__giXBvuL01wHMtF5XOnt0qJ1EHY/edit#slide=id.p)

---

16º Encontro - Talks e Networking no [Meetup.com](https://www.meetup.com/rubyfloripa/events/241172732/).

Quer ficar por dentro dos próximos meetups? [Faça parte da nossa comunidade](/faca-parte).

Também se inscreva no [canal RubyFloripa no Youtube](https://www.youtube.com/channel/UCnSXXzLeSmpmTILPdkkjDfg) e confira o que rolou até agora.

---
layout: post
title: "Workshop: Iniciando no Ruby - Dia 1"
excerpt: "Primeiro workshop focado em iniciantes para aprender Ruby"
categories: articles
tags: [ruby, slides, meetup, workshop]
comments: true
share: true
---

Pensando nas pessoas que queriam aprender Ruby como primeira linguagem de programação ou aprender uma nova, fizemos o workshop para iniciantes em Ruby na [CTC da UFSC](http://portal.ctc.ufsc.br) e, foi muito legal ver a disposição do pessoal em acordar bem cedo em um sábado para aprender mais!

![Início do workshop](https://secure.meetupstatic.com/photos/event/c/c/8/3/600_471232355.jpeg)

Todo código apresentado nos slides e durante o workshop estão disponíveis no [nosso repositório Workshop no Gitlab](https://gitlab.com/rubyfloripa/workshop).

![Participantes do workshop](https://secure.meetupstatic.com/photos/event/c/c/8/7/600_471232359.jpeg)

Tivemos o cuidado de deixar bastantes explicações nos slides para caso quem participou queira revisar ou que não pôde participar queira se inteirar:

<iframe src="https://docs.google.com/presentation/d/1kHy70RnxQB8ZVkL963vK_CZyP7iejijEnQ6KIhD4sYQ/preview" style="width:100%; height:440px; border:0;"></iframe>

Esperamos vocês no 2o. dia do workshop! Ainda sem data definida :)

![Pessoal que participou do workshop](https://secure.meetupstatic.com/photos/event/c/c/2/3/600_471232259.jpeg)

Agradecemos a todos que participaram e ao apoio dos tutores por ajudarem o pessoal nesse início.

---

Workshop iniciando no Ruby - Dia 1 no [Meetup.com](https://www.meetup.com/rubyfloripa/events/250526038/).

Quer ficar por dentro dos próximos meetups? [Faça parte da nossa comunidade](/faca-parte).

Também se inscreva no [canal RubyFloripa no Youtube](https://www.youtube.com/channel/UCnSXXzLeSmpmTILPdkkjDfg) e confira o que rolou até agora.


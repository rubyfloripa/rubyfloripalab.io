---
layout: post
title: "21º Encontro - Talks e Networking"
excerpt: "Última edição do RubyFloripa de 2018"
categories: articles
tags: [ruby, elixir, comunicacao, slides, meetup, networking]
comments: true
share: true
---

O 21º encontro do RubyFloripa aconteceu na [Darwin Startups](https://darwinstartups.com/) com coffee-break patrocinado pela [Resultados Digitais](https://resultadosdigitais.com.br)! \o/

### Como a comunicação mudou minha carreira

Nicholas Eduardo - @nicholasess

<iframe src="https://docs.google.com/presentation/d/1VYvlQTjg6KIbVtChwxliE9z_FuVo_7T2QHcssED4okk/preview" style="width:100%; height:440px; border:0;"></iframe>

### Assumindo uma app legada em Rails e introduzindo Elixir

Weverton Timoteo - @wevtimoteo

{% slideshare 1wIFReVUePNH2 %}

---

21º Encontro - Talks e Networking no [Meetup.com](https://www.meetup.com/rubyfloripa/events/256630593/).

Quer ficar por dentro dos próximos meetups? [Faça parte da nossa comunidade](/faca-parte).

Também se inscreva no [canal RubyFloripa no Youtube](https://www.youtube.com/channel/UCnSXXzLeSmpmTILPdkkjDfg) e confira o que rolou até agora.



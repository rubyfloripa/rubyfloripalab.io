---
layout: post
title: "22º Encontro - Talks e Networking"
excerpt: "Primeira edição do RubyFloripa de 2019"
categories: articles
tags: [ruby, comunicacao, slides, meetup, networking]
comments: true
share: true
---

O 22º encontro do RubyFloripa aconteceu na [Darwin Startups](https://darwinstartups.com/) com coffee-break patrocinado pela [TopTal](https://toptal.com)! \o/

### Guidelines for Onboarding Developers

Jônatas Paganini - @jonatasdp

[Aguardando slides]

### Práticas para implantar uma cultura de qualidade de código nos times

João Paulo Lethier - @jplethier

{% slideshare 6ndzqc3N6rSEtR %}

---

22º Encontro - Talks e Networking no [Meetup.com](https://www.meetup.com/rubyfloripa/events/258299262/).

Quer ficar por dentro dos próximos meetups? [Faça parte da nossa comunidade](/faca-parte).

Também se inscreva no [canal RubyFloripa no Youtube](https://www.youtube.com/channel/UCnSXXzLeSmpmTILPdkkjDfg) e confira o que rolou até agora.

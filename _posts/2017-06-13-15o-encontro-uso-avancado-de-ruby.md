---
layout: post
title: "15º Encontro - Uso avançado de Ruby"
excerpt: "O que rolou na 15ª edição do RubyFloripa com as talks apresentadas no dia."
categories: articles
tags: [ruby, slides, meetup, advanced, ast, lisp, monolith]
comments: true
share: true
---

O 15º encontro do RubyFloripa aconteceu na [Celta (Parque Tecnológico)](http://www.celta.org.br), HQ da [GeekHunter](http://geekhunter.com.br).

Segue slides das talks apresentadas no dia:

### Como não fazer da sua aplicação Rails um grande monolito

Bruno Almeida - @wwwbruno

{% slideshare d4FRojd8fHytWT %}

### O que é DSL e o que não é?

Leandro Parazito - @parazito

{% slideshare eT4KiFQzZFgnCW %}

### Escrevendo um pequeno interpretador LISP com menos de 30 linhas

Jonatas Paganini - @jonatasdp

Não teve slide, mas teve livecoding, o resultado pode ser encontrado nos repositórios:

* [Parser Playground](https://github.com/jonatas/parser_playground)
* [FAST](https://github.com/jonatas/fast) (Find in Abstract Syntax Tree)

---

15º Encontro - Talks e Networking no [Meetup.com](https://www.meetup.com/rubyfloripa/events/240253153/).

Quer ficar por dentro dos próximos meetups? [Faça parte da nossa comunidade](/faca-parte).

Também se inscreva no [canal RubyFloripa no Youtube](https://www.youtube.com/channel/UCnSXXzLeSmpmTILPdkkjDfg) e confira o que rolou até agora.

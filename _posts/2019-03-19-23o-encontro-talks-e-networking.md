---
layout: post
title: "23º Encontro - Talks e Networking"
excerpt: "Primeira edição do RubyFloripa no continente com parceria do grupo White Stone Developers"
categories: articles
tags: [ruby, comunicacao, slides, meetup, networking, golang, iot]
comments: true
share: true
---

O 23º encontro do RubyFloripa aconteceu na [Atrium Offices](http://atriumpedrabranca.com.br) com coffee-break patrocinado pela [Eurotec Nutrition](https://euronutri.com.br)! \o/

Essa foi a primeira edição que fizemos no continente e primeira parceria com o grupo [White Stone Developers](https://whitestonedev.com.br) que tem IoT como foco, sendo o último slot de talk reservado para falar especificamente desse assunto :)

O meetup foi transmitido ao vivo e a gravação está disponível!

<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fwhitestonedevelopers%2Fvideos%2F374747750054104%2F&show_text=0&width=560" width="713" height="401" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>

### A multiplicação dos Devs! Dicas para empresas e devs se relacionarem melhor

[Isaac Souza](https://www.linkedin.com/in/isaacfsouza)

{% slideshare 83YqawPSHx0Yby %}

### Introducing a microservice architecture

Leandro Parazito - @parazito

{% slideshare 2iDE0Hew2n1SKp %}

### Internet das coisas: Desenvolver e empreender

[Vinícius Sebastião](https://www.linkedin.com/in/viniciusconex)

* [Slides](https://spark.adobe.com/page/DFq0m9JTzwQwl)

---

23º Encontro - Talks e Networking no [Meetup.com](https://www.meetup.com/rubyfloripa/events/259325157).

Quer ficar por dentro dos próximos meetups? [Faça parte da nossa comunidade](/faca-parte).

Também se inscreva no [canal RubyFloripa no Youtube](https://www.youtube.com/channel/UCnSXXzLeSmpmTILPdkkjDfg) e confira o que rolou até agora.

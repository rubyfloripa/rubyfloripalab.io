---
layout: post
title: "[video] Como rubistas estão escalando suas aplicações"
excerpt: "No dia 12 de abril a galera do RubyFloripa se reuniu para conversar sobre Ruby e seu escosistema"
categories: articles
tags: [video, meetups]
comments: true
share: true
---

{% youtube T5SgeBhpXss %}

Já fizemos hack sessions e também palestras, e neste encontro fizemos um formato um pouco diferente!

Bate papo com quem está na estrada com Ruby a um bom tempo! Presenças confirmadas para o bate papo:

* [Bruno Ghisis](https://twitter.com/brunogh) - Resultados Digitais
* [Elber Ribeiro](https://twitter.com/dynaum) -  Parafuzo
* [Gabriel Mazzetto](https://twitter.com/brodock) - GitLab
* [Lucas Prim](https://twitter.com/brodock) - SumOne
* [Weverton Timoteo](https://twitter.com/wevtimoteo)- AutoCargo

Alguns tópicos abordados:

Como conheceu a linguagem? Como foi o processo de aprendizado? Aonde Rails se
mostrou superior em relação a outras tecnologias?Experiências em produção,
problemas e soluções?Quais os pontos negativos da linguagem;Qual o futuro de
Ruby?

O evento foi generosamente gravado pelo @derekstavis!

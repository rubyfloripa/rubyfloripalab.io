---
layout: post
title: "Workshop: Iniciando no Ruby - Dia 2"
excerpt: "Segunda edição do workshop focado em iniciantes que querem aprender Ruby"
categories: articles
tags: [ruby, slides, meetup, workshop]
comments: true
share: true
---

Continuando nossa série de workshops, preparamos um conteúdo cobrindo mais funcionalidades do Ruby e explicando alguns conceitos. O meetup aconteceu no espaço de co-working [Cool2Work](http://cool2work.com.br/) seguindo o mesmo modelo do primeiro dia (em um Sábado de manhã).

Todo código apresentado nos slides e durante o workshop estão disponíveis no [nosso repositório Workshop no Gitlab](https://gitlab.com/rubyfloripa/workshop).

Continuamos com os slides com bastantes explicações quem não conseguiu participar ou quem quiser revisar o conteúdo, possa acompanhar:

<iframe src="https://docs.google.com/presentation/d/1fsc7b-2wy_UYjT7aoZ2Rv0hTxgpTSeKMqXVRyVbqhPU/preview" style="width:100%; height:440px; border:0;"></iframe>

Ainda estamos preparando um conteúdo avançado como metaprogramming, threads e mais detalhes da linguagem.

Agradecemos a todos que participaram e ao apoio dos tutores por continuarem apoiando a comunidade.

---

Workshop iniciando no Ruby - Dia 2 no [Meetup.com](https://www.meetup.com/rubyfloripa/events/251652064).

Quer ficar por dentro dos próximos meetups? [Faça parte da nossa comunidade](/faca-parte).

Também se inscreva no [canal RubyFloripa no Youtube](https://www.youtube.com/channel/UCnSXXzLeSmpmTILPdkkjDfg) e confira o que rolou até agora.



---
layout: post
title: "14º Encontro - Testing Night"
excerpt: "O que rolou na 14ª edição do RubyFloripa com as talks apresentadas no dia."
categories: articles
tags: [ruby, slides, meetup, testing, rspec]
comments: true
share: true
---

O 14º encontro do RubyFloripa aconteceu na Resultados Digitais (@resdigitais).

Segue slides das talks apresentadas no dia:

### Quero testar meu código, mas não sei como

Leandro Parazito (@parazito) e Weverton Timoteo (@wevtimoteo)

{% slideshare GNPGxYlXYp2DNS %}

### Melhorando a legibilidade de seus testes escritos com RSpec

Luiz Cezer Marrone - @lccezinha

{% speakerdeck 4180295b9cbb46d5ab0e1a6de7eb1bbf %}

### Como estruturar page objects utilizando SitePrism

Leonardo Giacomini - @LGiacomini92

{% slideshare HglPTkVhOpdIFE %}

Ele também escreveu um [post no blog ShipIt](http://shipit.resultadosdigitais.com.br/blog/como-estruturar-page-objects-utilizando-siteprism/) da Resultados Digitais!

---

14º Encontro - Testing Night no [Meetup.com](https://www.meetup.com/rubyfloripa/events/238837005/).

Quer ficar por dentro dos próximos meetups? [Faça parte da nossa comunidade](/faca-parte).

Também se inscreva no [canal RubyFloripa no Youtube](https://www.youtube.com/channel/UCnSXXzLeSmpmTILPdkkjDfg) e confira o que rolou até agora.

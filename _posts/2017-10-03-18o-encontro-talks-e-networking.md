---
layout: post
title: "18º Encontro - Talks e Networking"
excerpt: "O que rolou na 18ª edição do RubyFloripa com as talks apresentadas no dia."
categories: articles
tags: [ruby, slides, meetup, networking]
comments: true
share: true
---

O 18º encontro do RubyFloripa aconteceu na [Resultados Digitais](https://www.resultadosdigitais.com.br).

### Building (cloudless) Enterprise Applications with Ruby

Gabriel Mazetto - @brodock

{% speakerdeck 655b4ed0aca540e08263618b34f4378d %}

### DevOps - Quebrando velhos paradigmas!

Pery Lemke - @perylemke

{% slideshare MkqKrwk2qVg5bN %}

---

18º Encontro - Talks e Networking no [Meetup.com](https://www.meetup.com/rubyfloripa/events/242262291/).

Quer ficar por dentro dos próximos meetups? [Faça parte da nossa comunidade](/faca-parte).

Também se inscreva no [canal RubyFloripa no Youtube](https://www.youtube.com/channel/UCnSXXzLeSmpmTILPdkkjDfg) e confira o que rolou até agora.

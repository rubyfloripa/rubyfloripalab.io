---
layout: post
title: "11º Encontro - Talks e Networking"
excerpt: "O que rolou na 11ª edição do RubyFloripa com as talks apresentadas no dia."
categories: articles
tags: [ruby, slides, meetup]
comments: true
share: true
---

O 11º encontro do RubyFloripa aconteceu na Resultados Digitais (@resdigitais).

![Talk do @lccezinha](https://secure.meetupstatic.com/photos/event/b/0/8/4/600_456465188.jpeg)

Segue slides das talks apresentadas no dia:

### Rails Girls: Dando oportunidade e mudando vidas

Lívia Amorim (@liviavamorim) e Ana Rodrigues (@anacaet)

{% speakerdeck df132cb9cec94809b2ac4b2fe8a16fb6 %}

### Gerenciando dependências front-end no Rails

Weverton Timoteo - @wevtimoteo

{% slideshare c2CYuAeJbr0HDX %}

### Melhorando o design do seu código Ruby

Luiz Cezer Marrone - @lccezinha

{% speakerdeck 7806dc4d595c4ee9ad0fb676088af79d %}

---

No final tiramos essa foto com a raça:

![Raça RubyFloripa](https://secure.meetupstatic.com/photos/event/b/0/7/8/600_456465176.jpeg)

11º Encontro - Talks e Networking no [Meetup.com](https://www.meetup.com/rubyfloripa/events/235484496/).

Quer ficar por dentro dos próximos meetups? [Faça parte da nossa comunidade](/faca-parte).

Também se inscreva no [canal RubyFloripa no Youtube](https://www.youtube.com/channel/UCnSXXzLeSmpmTILPdkkjDfg) e confira o que rolou até agora.

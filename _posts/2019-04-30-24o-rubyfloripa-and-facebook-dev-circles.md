---
layout: post
title: "24º Encontro - RubyFloripa + Facebook Developer Circles"
excerpt: "Mais uma parceria, dessa vez com o grupo Facebook Developer Circles"
categories: articles
tags: [ruby, comunicacao, slides, meetup, networking, facebook, feature flipper, reactjs, devops]
comments: true
share: true
---

O 24º encontro do RubyFloripa aconteceu na [Zygo Tecnologia](https://site.zygotecnologia.com/home/) (Zygo é o novo nome da SumOne).

Seguindo a ideia do meetup anterior, dessa vez fizemos parceria com o grupo [Facebook Developer Circles](https://www.facebook.com/groups/devcfloripa), que além de patrocinar o nosso coffee-break, também patrocinou a confecção dos nossos copos personalizados da [Meu Copo Eco](http://meucopoeco.com.br):

![Meu Copo Eco - RubyFloripa + Facebook Developer Circles](https://secure.meetupstatic.com/photos/event/1/f/4/9/600_480968009.jpeg)

O meetup foi transmitido ao vivo e a gravação está disponível no nosso canal do YouTube!

{% youtube UygdqY-qbLI %}

Aproveite e [se inscreva](http://www.youtube.com/c/RubyFloripa)!

---

Seguem as palestras apresentadas no dia:

### A Saga do Continuous Delivery - Do Make ao Deploy Automatizado!

Pery Lemke - @perylemke

{% speakerdeck e122f91fbeaa411e850ba40164564fad %}

### Feature flipping, desenvolvimento e produto

Evandro Sasse - [@evsasse](https://github.com/evsasse)

<iframe src="https://docs.google.com/presentation/d/1HCiFPCCfuF1Vb7Ot4GvOXzHV700DBGldmV7qRDg-_-A/preview" style="width:100%; height:440px; border:0;"></iframe>

### Building a ReactJS Front-end for a Legacy Rails app

Matt Rider - [@mattwr18](https://github.com/mattwr18)

<iframe src="https://docs.google.com/presentation/d/1ZvjYs8iP0yqav5Q9xm5S1mVEUsrxyoaKcs-tQH6NZlg/preview" style="width:100%; height:440px; border:0;"></iframe>

---

24º Encontro - RubyFloripa + Facebook Developer Circles no [Meetup.com](https://www.meetup.com/rubyfloripa/events/259935239).

Quer ficar por dentro dos próximos meetups? [Faça parte da nossa comunidade](/faca-parte).

Também se inscreva no [canal RubyFloripa no Youtube](https://www.youtube.com/c/rubyfloripa) e confira o que rolou até agora.


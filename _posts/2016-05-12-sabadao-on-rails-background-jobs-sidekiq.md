---
layout: post
title: "[video] Sabadão On Rails - Background Jobs (com Sidekiq)"
excerpt: "Nesta talk @jplethier fala sobre boas práticas no uso de background jobs, usando o Sidekiq como exemplo"
categories: articles
tags: [video, meetups, background jobs, sidekiq]
comments: true
share: true
---

{% youtube q_VZKeQ1l1k %}

Um dos temas abordados nesse Sabadão on Rails foi o uso de background jobs, boas práticas e como utilizar com o Sidekiq, apresentado pelo João Paulo Lethier(@jplethier).

{% slideshare NXlR7hPZl3xNjd %}

Nessa talk foram mostrados vários exemplos de uso do sidekiq, podendo inclusive acessar o código de exemplo no [https://github.com/jplethier/sidekiq_sample](https://github.com/jplethier/sidekiq_sample).

---

Não deixe de se inscrever no [canal RubyFloripa no Youtube](https://www.youtube.com/channel/UCnSXXzLeSmpmTILPdkkjDfg) e confira as outras talks que gravamos.

Um super obrigado ao @derekstavis que generosamente gravou, editou e publicou todo o vídeo.

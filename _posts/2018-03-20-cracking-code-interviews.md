---
layout: post
title: "Cracking code interviews"
excerpt: "Aprendendo mais sobre Ruby quebrando desafios e implementando algoritmos"
categories: articles
tags: [ruby, slides, meetup, code interview, code challenge]
comments: true
share: true
---

Com a ideia de trabalhar a colaboração e se aprofundar nos conhecimentos em Ruby, promovemos um meetup focado em reunir os membros formando grupos de 3-5 pessoas para desenvolver algum desafio ou algoritmo.

![Grupo Cracking Code interview](https://secure.meetupstatic.com/photos/event/d/8/e/e/600_469435534.jpeg)

O meetup aconteceu na [Resultados Digitais](https://resultadosdigitais.com.br), além disso a RD patrocinou nosso coffee-break a [Exact Sales](https://www.exactsales.com.br) patrocinou beer!

Inicialmente pegamos alguns testes que as empresas pedem para estudá-los a fundo formando pequenos grupos para compartilhar conhecimento. Durante o meetup, cada grupo acabou escolhendo um desafio diferente e, quem nunca tinha programado em Ruby, usou a experiência para aprender.

Algumas plataformas de desafios utilizadas:

* [Exercism](https://exercism.io/)
* [HackerRank](https://www.hackerrank.com/)
* [CoderByte](https://coderbyte.com/)

Além das plataformas, adicionamos alguns challenges no repositório [coding-dojo](https://github.com/RubyFloripa/coding-dojo) no Github.

![https://secure.meetupstatic.com/photos/event/d/9/3/2/600_469435602.jpeg](https://secure.meetupstatic.com/photos/event/d/9/3/2/600_469435602.jpeg)

Veja mais fotos do evento no [Meetup.com](https://www.meetup.com/rubyfloripa/events/248070874/).

---

Quer ficar por dentro dos próximos meetups? [Faça parte da nossa comunidade](/faca-parte).

Também se inscreva no [canal RubyFloripa no Youtube](https://www.youtube.com/channel/UCnSXXzLeSmpmTILPdkkjDfg) e confira o que rolou até agora.

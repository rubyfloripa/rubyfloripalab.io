---
layout: post
title: "[video] Sabadão On Rails - Concurrency"
excerpt: "Nesta talk @jonatasdp e @jaisonerick compartilham sua experiência com concorrência em Ruby."
categories: articles
tags: [video, meetups, concurrency]
comments: true
share: true
---

No último meetup: [Sabadão on Rails](https://www.meetup.com/rubyfloripa/events/230489351/) tivemos uma série de talks avançadas sobre Ruby.

{% youtube 97xO2JhoVGY %}

Um dos temas foi [Concurrency](https://en.wikipedia.org/wiki/Concurrency_(computer_science)) em processos apresentado por Jônatas Paganini (@jonatasdp) e @jaisonerick.

{% slideshare 1XOxCGgBwQ8WoE %}

No episódio mostramos como criar uma queue e consumir ela em outra thread. Usamos o famoso termo `Xunda` para não entrar em conflito com nomes.

```ruby
require 'thread'
require 'zip'
require 'tempfile'

class Xunda < Queue
  def initialize
    super
    @thread = Thread.new do
      while true
        if line = pop
          puts "recebeu: #{line}"
        end
      end
    end
  end
end
q = Xunda.new
q << "a"
sleep 1
q << "b"
sleep 2
q << "c"
sleep 4
q << "bye"
```

Não deixe de se inscrever no [canal RubyFloripa no Youtube](https://www.youtube.com/channel/UCnSXXzLeSmpmTILPdkkjDfg) e confira as outras talks que gravamos.

Um super obrigado ao @derekstavis que generosamente gravou, editou e publicou todo o vídeo.

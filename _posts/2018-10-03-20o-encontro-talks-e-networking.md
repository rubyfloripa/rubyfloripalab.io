---
layout: post
title: "20º Encontro - Talks e Networking"
excerpt: "O que rolou na 20ª edição do RubyFloripa com as talks apresentadas no dia."
categories: articles
tags: [ruby, slides, meetup, networking]
comments: true
share: true
---

O 20º encontro do RubyFloripa aconteceu na [SumOne](http://sumone.com.br) e tivemos uma surpresa bem agradável: logo após [TheConf](https://www.theconf.club/) tivemos todas as talks em inglês, o que foi ótimo para treinarmos tanto como palestrantes como ouvintes!

### Building your own flavored Markdown with Ruby

Gabriel Mazetto - @brodock

{% speakerdeck 655b4ed0aca540e08263618b34f4378d %}

### AgileVentures - Crowdsourced Learning and Software Development as part of a Distributed Team

Matthew Rider - @mattwr18

<iframe src="https://docs.google.com/presentation/d/1FXJrIh4vHAbyhAKef652vgT98iRwM1EIUkslqR-CfHg/preview" style="width:100%; height:440px; border:0;"></iframe>

### Architecting Rails apps

Iago Dahlem Lorensini - @iagodahlem

{% speakerdeck c85af157282345129c02e1a49990498d %}

---

20º Encontro - Talks e Networking no [Meetup.com](https://www.meetup.com/rubyfloripa/events/255169247/).

Quer ficar por dentro dos próximos meetups? [Faça parte da nossa comunidade](/faca-parte).

Também se inscreva no [canal RubyFloripa no Youtube](https://www.youtube.com/channel/UCnSXXzLeSmpmTILPdkkjDfg) e confira o que rolou até agora.


---
layout: post
title: "17º Encontro - Coding Dojo"
excerpt: "Como foi o Coding Dojo realizado na 17ª edição."
categories: articles
tags: [ruby, meetup, coding dojo]
comments: true
share: true
---

O Coding Dojo do 17º encontro do RubyFloripa aconteceu no [SENAI](http://sc.senai.br/), utilizamos uma sala de aula para realizarmos a sessão.

### Introdução

O Dojo foi conduzido pelo Leandro Parazito (@parazito) e Weverton Timoteo (@wevtimoteo), primeiro foi apresentado uma introdução sobre o que é um Coding Dojo e os modelos mais conhecidos:

{% slideshare qgmntun9KwVeNl %}

### Modelo de Dojo

Utilizamos o modelo onde um codifica (piloto), um coordena e todos na sala tentam resolver o problema em questão.

![Coding Dojo 1](https://secure.meetupstatic.com/photos/event/4/9/e/6/600_464178918.jpeg)

Seguimos a seguinte ordem:

Piloto -> Coordenador -> Grupo

Praticamente todos da sala conseguiram codificar, participando do desafio. Focamos no público iniciante ou que nunca tiveram contato com Ruby.

### Desafio

Resolvemos o desafio [FizzBuzz](https://github.com/RubyFloripa/coding-dojo/blob/master/sessions/31-08-2017/fizzbuzz/README.md) e [Calculator](https://github.com/RubyFloripa/coding-dojo/blob/master/challenges/calculator/README.md), você pode conferir o código no repositório do [RubyFloripa no Github](https://github.com/RubyFloripa/coding-dojo).

![Coding Dojo 2](https://secure.meetupstatic.com/photos/event/4/9/e/4/600_464178916.jpeg)

### Conclusão

No final fizemos uma mini-retrospectiva para saber o que a galera achou, esse foi o resultado:

![Mini retrospectiva](https://secure.meetupstatic.com/photos/event/4/9/e/3/600_464178915.jpeg)

---

17º Encontro - Coding Dojo no [Meetup.com](https://www.meetup.com/rubyfloripa/events/242202574/).

Quer ficar por dentro dos próximos meetups? [Faça parte da nossa comunidade](/faca-parte).

Também se inscreva no [canal RubyFloripa no Youtube](https://www.youtube.com/channel/UCnSXXzLeSmpmTILPdkkjDfg) e confira o que rolou até agora.

---
layout: post
title: "26º Encontro - Talks e Networking"
excerpt: "O que rolou na 26º edição do RubyFloripa com as talks apresentadas no dia."
categories: articles
tags: [ruby, slides, meetup, networking, devkit, rails]
comments: true
share: true
---

O 26º encontro do RubyFloripa aconteceu no [Mercado Livre](https://www.mercadolivre.com.br).

Contamos com o apoio da [Toptal](https://www.toptal.com) patrocinando o nosso coffee break \o/

O meetup foi transmitido ao vivo e a gravação está disponível no nosso canal do YouTube!

{% youtube AuEQZGLKjHA %}

Aproveite e [se inscreva](http://www.youtube.com/c/RubyFloripa)!

---

Seguem as palestras apresentadas no dia:

### Primeiros passos no Ruby on Rails

João Moura Lima - [LinkedIn](https://www.linkedin.com/in/joao-lima-dev)

<iframe src="https://docs.google.com/file/d/1xfyM-idgiJZzq1fQTKaP1mT9JMhV-FLcBIoMfTCGKIQ/preview" style="width:100%; height:440px; border:0;"></iframe>

### Why your company should have a Developer's Kit and how to build one

Gabriel Mazetto - @brodock

{% speakerdeck e4e41fac2e4d419495ef33e919c351e2 %}

### [Lightning talk] Rails in 2019

Marcelo Kopmann - @HeyMarkKop

<iframe src="https://docs.google.com/file/d/1d8SST0bhF-O0OEsclPWFsDKQsdAGjpTlEm-l_XSHvzI/preview" style="width:100%; height:440px; border:0;"></iframe>

A [GeekHunter](https://www.geekhunter.com.br) também presentou o palestrante da lightning talk com o livro Domain-Driven Design do Eric Evans. \o/

---

### Sorteios

No final do evento sorteamos alguns copos do RubyFloripa e uma licença completa de todos os produtos da [JetBrains](https://www.jetbrains.com), o sorteado foi: [Gabriel Mazetto](https://twitter.com/brodock)!

![Sorteio JetBrains - 26o meetup](/images/26o_rubyfloripa_jetbrains.jpeg)
---

26º Encontro - RubyFloripa - Talks e Networking [Meetup.com](https://www.meetup.com/rubyfloripa/events/263061972/).

Quer ficar por dentro dos próximos meetups? Faça parte da nossa comunidade em [RubyFloripa no Meetup.com](http://www.meetup.com/rubyfloripa).

Também se inscreva no [canal RubyFloripa no Youtube](https://www.youtube.com/c/rubyfloripa) e confira o que rolou até agora.


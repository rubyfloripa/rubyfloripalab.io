---
layout: post
title: "12º Encontro - Iniciando no Ruby e Networking"
excerpt: "O que rolou na 12ª edição do RubyFloripa com as talks apresentadas no dia."
categories: articles
tags: [ruby, slides, meetup, arduino, iot]
comments: true
share: true
---

O 12º encontro do RubyFloripa aconteceu na Resultados Digitais (@resdigitais).

Segue slides das talks apresentadas no dia:

### Prototipação rápida com Ruby on Rails e Arduino

Daner dos Reis - @danerdosreis

{% slideshare HmCnS1wfbHelYI %}

---

Quer ficar por dentro dos próximos meetups? [Faça parte da nossa comunidade](/faca-parte).

Também se inscreva no [canal RubyFloripa no Youtube](https://www.youtube.com/channel/UCnSXXzLeSmpmTILPdkkjDfg) e confira o que rolou até agora.

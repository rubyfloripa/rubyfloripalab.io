---
layout: page
title: Branding
excerpt: "RubyFloripa é a comunidade de Usuários Ruby e Rails de Florianópolis-SC e região."
modified: 2018-04-30T00:38:38.564948-03:00
---

## Marca/Logo

Caso precise da nossa marca/logo para divulgação ou algo do gênero, você pode encontrar no [repositório branding no Gitlab](https://gitlab.com/rubyfloripa/branding).

## Template Slides

Está preparando uma talk para nosso próximo meetup? Fique à vontade para usar [nosso template de slides](https://docs.google.com/presentation/d/1Zwo_JuOrZgB0yyzTq94rNT5EDse14SCctkkaZatyt-0).

<iframe src="https://docs.google.com/presentation/d/1Zwo_JuOrZgB0yyzTq94rNT5EDse14SCctkkaZatyt-0/preview" style="width:100%; height:440px; border:0;"></iframe>

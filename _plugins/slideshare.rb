class SlideShare < Liquid::Tag
  PATTERN = /^\s*([^\s]+)(?:\s+([^\s]+)\s*)?/
  DEFAULT_RATIO = 'wide'

  def initialize(tagName, markup, tokens)
    if markup =~ PATTERN
      @id = $1
      sizes($2)
    else
      raise 'No SlideShare embedded KEY provided in the "slideshare" tag'
    end
  end

  def render(context)
    %Q(<iframe width="#{@width}" height="#{@height}" src="//www.slideshare.net/slideshow/embed_code/key/#{@id}" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen></iframe>)
  end

  def sizes(ratio)
    ratio ||= DEFAULT_RATIO

    case ratio
    when 'wide', 'widescreen'
      @width = 560
      @height = 346
    when 'normal'
      @width = 560
      @height = 450
    else
      raise 'Invalid aspect ratio provided in the "slideshare" tag'
    end
  end

  Liquid::Template.register_tag('slideshare', self)
end

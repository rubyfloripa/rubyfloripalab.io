class YouTube < Liquid::Tag
  PATTERN = /^\s*([^\s]+)(?:\s+([^\s]+)\s*)?/
  DEFAULT_RATIO = 'wide'

  def initialize(tagName, markup, tokens)
    if markup =~ PATTERN
      @id = $1
      sizes($2)
    else
      raise 'No YouTube ID provided in the "youtube" tag'
    end
  end

  def render(context)
    %Q(<iframe width="#{@width}" height="#{@height}" src="https://www.youtube-nocookie.com/embed/#{@id}" frameborder="0" allowfullscreen></iframe>)
  end

  def sizes(ratio)
    ratio ||= DEFAULT_RATIO

    case ratio
    when 'wide', 'widescreen'
      @width = 560
      @height = 315
    when 'normal'
      @width = 560
      @height = 420
    else
      raise 'Invalid aspect ratio provided in the "slideshare" tag'
    end
  end

  Liquid::Template.register_tag('youtube', self)
end

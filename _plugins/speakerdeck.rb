class SpeakerDeck < Liquid::Tag
  PATTERN = /^\s*([^\s]+)(?:\s+([^\s]+)\s*)?/
  DEFAULT_RATIO = 'wide'

  def initialize(tagName, markup, tokens)
    if markup =~ PATTERN
      @id = $1
      sizes($2)
    else
      raise 'No SpeakerDeck ID provided in the "speakerdeck" tag'
    end
  end

  def render(context)
    %Q(<script async class="speakerdeck-embed" data-id="#{@id}" data-ratio="#{@ratio}" src="//speakerdeck.com/assets/embed.js"></script>)
  end

  def sizes(ratio)
    ratio ||= DEFAULT_RATIO

    case ratio
    when 'wide', 'widescreen'
      @ratio = '1.77777777777778'
    when 'normal'
      @ratio = '1.33333333333333'
    else
      raise 'Invalid aspect ratio provided in the "speakerdeck" tag'
    end
  end

  Liquid::Template.register_tag('speakerdeck', self)
end

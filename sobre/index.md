---
layout: page
title: Sobre o RubyFloripa
excerpt: "RubyFloripa é uma iniciativa para difundir a cultura Ruby em Floripanópolis"
modified: 2014-08-08T19:44:38.564948-04:00
image:
  feature: rubyfloripa_people.jpg
---

Esse grupo é destinado a todos que desejam aprender e difundir conhecimento sobre Ruby & Rails, fazer networking e amigos. Teremos (por enquanto) dois tipos de eventos:

### Hacking Sessions:
A melhor maneira de se tornar um desenvolvedor melhor é praticando. Nas Hacking Sessions iremos trabalhar em equipe para resolver um desafio proposto.

### Lighting talks:
Palestras curtas e objetivas (máximo 15 min). Iniciantes terão oportunidade de treinar seus skills de apresentação :)

[Junte-se a nós](http://www.meetup.com/rubyfloripa)
